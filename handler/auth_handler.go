package handler

import (
	"net/http"

	"gitlab.com/OrsonDC/solar-planner-api/store/model"
	"gitlab.com/OrsonDC/solar-planner-api/validation"
)

func authRoutes() {
	router.HandleFunc("/auth/login", login).Methods("POST")
	router.HandleFunc("/auth/signup", signup).Methods("POST")
}

func login(w http.ResponseWriter, r *http.Request) {
	var account model.Account
	readBytes(r, &account)

	token, err := s.Auth().Login(&account)
	if err != nil {
		v := validation.Validation{}
		v.AddError("Invalid email or password")
		respond(w, v, http.StatusBadRequest)

		return
	}

	respond(w, token, http.StatusOK)
}

func signup(w http.ResponseWriter, r *http.Request) {
	var account model.Account
	readBytes(r, &account)

	v, err := validation.ValidateAccount(&account)
	if err != nil {
		respondMsg(w, "Error: Could not create account", http.StatusBadRequest)
		return
	}
	if !v.IsValid() {
		respond(w, v, http.StatusBadRequest)
		return
	}

	createdAccount, err := s.Account().Get(account.Id)
	if err != nil {
		respondMsg(w, "Error: Could not create account", http.StatusBadRequest)
		return
	}

	respond(w, createdAccount, http.StatusCreated)
}
