VERSION=`git describe --always`

echo $VERSION

docker build . -t orsondc/solar-planner-api:dev && \
docker push orsondc/solar-planner-api:dev