FROM golang:1.16.13-alpine3.15 AS build

ENV PROJECT_DIR="/go/src/gitlab.com/OrsonDC/solar-planner-api"

RUN apk add --no-cache git
WORKDIR $PROJECT_DIR

COPY . $PROJECT_DIR

RUN go get -d -v ./... && \
    go install -v ./...

RUN go build -o /bin/solar-planner-api

FROM golang:alpine

ENV BIN_DIR="/bin"

WORKDIR $BIN_DIR

COPY --from=build $BIN_DIR/solar-planner-api $BIN_DIR/solar-planner-api
COPY --from=build /go/src/gitlab.com/OrsonDC/solar-planner-api/db.json $BIN_DIR/

EXPOSE 8085

CMD /bin/solar-planner-api

