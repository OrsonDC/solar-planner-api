module gitlab.com/OrsonDC/solar-planner-api

go 1.17

require (
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.6
	golang.org/x/crypto v0.0.0-20220518034528-6f7dac969898
)

require github.com/felixge/httpsnoop v1.0.1 // indirect
