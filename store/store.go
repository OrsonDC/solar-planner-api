package store

import (
	"gitlab.com/OrsonDC/solar-planner-api/store/model"
)

var store Store

type Store interface {
	Account() AccountStore
	Auth() AuthStore
	Token() TokenStore

	HelperStore
}

type AccountStore interface {
	Get(id int) (model.Account, error)
	GetAll() ([]model.Account, error)
	Create(account *model.Account) error
	Update(account *model.Account) error
	Delete(id int) error
}

type AuthStore interface {
	Login(account *model.Account) (model.Token, error)
}

type TokenStore interface {
	GetAll() ([]model.Token, error)
	DeleteAllByAccountId(accountId int) error
}

type HelperStore interface {
	IsDuplicate(table string, column string, field interface{}, id int) bool
}
