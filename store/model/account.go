package model

import "time"

type Account struct {
	Id         int       `json:"id"`
	Username   string    `json:"username"`
	Email      string    `json:"email"`
	Password   string    `json:"password"`
	CreateDate time.Time `json:"createDate"`
}
